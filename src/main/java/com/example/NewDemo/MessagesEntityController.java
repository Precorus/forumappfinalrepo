package com.example.NewDemo;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.*;
import java.io.Serializable;
import java.util.*;

@Named("dtMessagesEntityController")
@SessionScoped
public class MessagesEntityController implements Serializable {

    private List<MessagesEntity> messages;
    private List<MessagesEntity> updatedList;
    private String username;
    private String Text;


    @Inject
    private MessagesEntityService service;

    @PostConstruct
    public void init() {
        messages = service.getMessagesEntity();
    }

    public List<MessagesEntity> getMessages() {
        return messages;
    }

    public void saveMessage() throws Exception {
        service.setMessagesEntity(username, Text);
        updateMessages();
    }

    public void updateMessages() {

        if(messages.size() == 0 ) {
            Calendar calendar = new GregorianCalendar(2000,0,31);
            updatedList = service.getMessagesEntity2(calendar.getTime());
        }
        else{
            updatedList = service.getMessagesEntity2(messages.get(messages.size() - 1).getMessageTime());
        }

        if (updatedList != null && updatedList.size() > 0 ) {
            messages.addAll(updatedList);
        }
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }



}

package com.example.NewDemo;

import javax.annotation.Resource;
import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import javax.persistence.criteria.*;
import javax.transaction.UserTransaction;

public class MessagesEntityService implements Serializable {

    @PersistenceContext
    private static EntityManager entityMgrObj = Persistence.createEntityManagerFactory("mydb").createEntityManager();
    public List<MessagesEntity> getMessagesEntity() {

        CriteriaBuilder criteriaBuilderObj = entityMgrObj.getCriteriaBuilder();
        CriteriaQuery<MessagesEntity> queryObj = criteriaBuilderObj.createQuery(MessagesEntity.class);

        Root<MessagesEntity> from = queryObj.from(MessagesEntity.class);
        CriteriaQuery<MessagesEntity> sortedSelectQuery = queryObj.select(from);

        sortedSelectQuery.orderBy(criteriaBuilderObj.desc(from.get("messageTime")));

        TypedQuery<MessagesEntity> SortedTypedQuery = entityMgrObj.createQuery(sortedSelectQuery).setMaxResults(20);
        List<MessagesEntity> SortedMessages = SortedTypedQuery.getResultList();

        Collections.reverse(SortedMessages);
        return SortedMessages;

    }

    public List<MessagesEntity> getMessagesEntity2(Date messageTime) {

        CriteriaBuilder criteriaBuilderObj = entityMgrObj.getCriteriaBuilder();
        CriteriaQuery<MessagesEntity> queryObj = criteriaBuilderObj.createQuery(MessagesEntity.class);

        Root<MessagesEntity> from = queryObj.from(MessagesEntity.class);

        Predicate predicate = criteriaBuilderObj.greaterThan(from.get("messageTime"), messageTime);
        CriteriaQuery<MessagesEntity> sortedSelectQuery = queryObj.select(from).where(predicate);

        sortedSelectQuery.orderBy(criteriaBuilderObj.desc(from.get("messageTime")));

        TypedQuery<MessagesEntity> SortedTypedQuery = entityMgrObj.createQuery(sortedSelectQuery).setMaxResults(20);
        List<MessagesEntity> SortedMessages = SortedTypedQuery.getResultList();

        Collections.reverse(SortedMessages);
        return SortedMessages;

    }

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext EntityManager em;
    public MessagesEntity setMessagesEntity(String username, String Text ) throws Exception{
        Date date = new Date();
        userTransaction.begin();

        MessagesEntity insertQuery = new MessagesEntity();
        insertQuery.setUsername(username);
        insertQuery.setText(Text);
        insertQuery.setMessageTime(new Timestamp(date.getTime()));

        em.persist(insertQuery);
        userTransaction.commit();
        return insertQuery;
    }


}

package com.example.NewDemo;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "messages")
public class MessagesEntity implements Serializable,Comparable<MessagesEntity>{
    private int idMessages;
    private String username;
    private String text;
    private Timestamp messageTime;

    public MessagesEntity() {}

    @Id
    @Column(name = "idMessages", nullable = false)
    public int getIdMessages() {
        return idMessages;
    }

    public void setIdMessages(int idMessages) {
        this.idMessages = idMessages;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 45)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "text", nullable = false, length = -1)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "message_time", nullable = false)
    public Timestamp getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(Timestamp messageTime) {
        this.messageTime = messageTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessagesEntity that = (MessagesEntity) o;

        if (idMessages != that.idMessages) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        if (messageTime != null ? !messageTime.equals(that.messageTime) : that.messageTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idMessages;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (messageTime != null ? messageTime.hashCode() : 0);
        return result;
    }


    @Override
    public int compareTo(MessagesEntity o) {
        return 0;
    }
}
